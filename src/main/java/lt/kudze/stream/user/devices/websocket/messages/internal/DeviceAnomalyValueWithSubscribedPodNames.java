package lt.kudze.stream.user.devices.websocket.messages.internal;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceAnomalyValue;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

import java.util.TreeSet;

public class DeviceAnomalyValueWithSubscribedPodNames extends DeviceAnomalyValue {
    public static final Serde<DeviceAnomalyValueWithSubscribedPodNames> serde = JsonSerdes.of(DeviceAnomalyValueWithSubscribedPodNames.class);

    private TreeSet<String> pods;

    public DeviceAnomalyValueWithSubscribedPodNames() {

    }

    public DeviceAnomalyValueWithSubscribedPodNames(DeviceAnomalyValue value, TreeSet<String> pods) {
        super(value);

        this.pods = pods;
    }

    public DeviceAnomalyValueWithSubscribedPodNames(DeviceAnomalyValue value, InternalDeviceSubscriptionValue subscriptionValue) {
        this(value, subscriptionValue.getPods());
    }

    public TreeSet<String> getPods() {
        return pods;
    }

    public void setPods(TreeSet<String> pods) {
        this.pods = pods;
    }
}
