package lt.kudze.stream.user.devices.websocket.messages.internal;

import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

import java.util.TreeSet;

public class DeviceSensorValueWithSubscribedPodNames extends InternalDeviceSensorValue {
    public static final Serde<DeviceSensorValueWithSubscribedPodNames> serde = JsonSerdes.of(DeviceSensorValueWithSubscribedPodNames.class);

    private TreeSet<String> pods;

    public DeviceSensorValueWithSubscribedPodNames() {

    }

    public DeviceSensorValueWithSubscribedPodNames(InternalDeviceSensorValue value, TreeSet<String> pods) {
        super(value);

        this.pods = pods;
    }

    public DeviceSensorValueWithSubscribedPodNames(InternalDeviceSensorValue value, InternalDeviceSubscriptionValue subscriptionValue) {
        this(value, subscriptionValue.getPods());
    }

    public TreeSet<String> getPods() {
        return pods;
    }

    public void setPods(TreeSet<String> pods) {
        this.pods = pods;
    }
}
