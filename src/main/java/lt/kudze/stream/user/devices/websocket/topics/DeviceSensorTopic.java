package lt.kudze.stream.user.devices.websocket.topics;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceSensorKey;
import lt.kudze.stream.user.devices.websocket.messages.device.DeviceSensorValue;
import lt.kudze.stream.user.devices.websocket.util.SystemUtil;
import org.apache.kafka.streams.kstream.Consumed;

public class DeviceSensorTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_DEVICE_SENSOR_TOPIC");
    public static final Consumed<DeviceSensorKey, DeviceSensorValue> CONSUMED = Consumed.with(DeviceSensorKey.serde, DeviceSensorValue.serde);
}
