package lt.kudze.stream.user.devices.websocket.messages.device;

import lt.kudze.stream.user.devices.websocket.serdes.StringSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceSensorValue {
    public static final Serde<DeviceSensorValue> serde = StringSerdes.from(
            DeviceSensorValue::toString,
            DeviceSensorValue::new
    );

    private String json;

    public DeviceSensorValue() {

    }

    public DeviceSensorValue(String json) {
        this.json = json;
    }

    public DeviceSensorValue(DeviceSensorValue value) {
        this.json = value.json;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    @Override
    public String toString() {
        return this.json;
    }
}
