package lt.kudze.stream.user.devices.websocket.messages.internal;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceSensorValue;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class InternalDeviceSensorValue extends DeviceSensorValue {
    public static final Serde<InternalDeviceSensorValue> serde = JsonSerdes.of(InternalDeviceSensorValue.class);

    private String date;

    public InternalDeviceSensorValue() {

    }

    public InternalDeviceSensorValue(DeviceSensorValue value, String date) {
        super(value);

        this.date = date;
    }

    public InternalDeviceSensorValue(InternalDeviceSensorValue value) {
        super(value);

        this.date = value.date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
