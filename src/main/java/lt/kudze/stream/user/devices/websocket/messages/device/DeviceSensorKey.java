package lt.kudze.stream.user.devices.websocket.messages.device;

import com.fasterxml.jackson.annotation.JsonProperty;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceSensorKey {
    public static final Serde<DeviceSensorKey> serde = JsonSerdes.of(DeviceSensorKey.class);

    @JsonProperty("deviceKey")
    private String deviceUuid;

    private String date;

    public DeviceSensorKey() {

    }

    public DeviceSensorKey(String deviceUuid, String date) {
        this.deviceUuid = deviceUuid;
        this.date = date;
    }

    public String getDeviceUuid() {
        return deviceUuid;
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
