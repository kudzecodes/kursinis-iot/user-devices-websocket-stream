package lt.kudze.stream.user.devices.websocket.serdes;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

public class NullSerdes {
    public static Serde<NullSerdes> serde = Serdes.<NullSerdes>serdeFrom(
            new Serializer<NullSerdes>() {
                @Override
                public byte[] serialize(String topic, NullSerdes data) {
                    return null;
                }
            },
            new Deserializer<NullSerdes>() {
                @Override
                public NullSerdes deserialize(String topic, byte[] data) {
                    return null;
                }
            }
    );
}
