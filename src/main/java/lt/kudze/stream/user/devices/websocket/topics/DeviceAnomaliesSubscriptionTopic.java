package lt.kudze.stream.user.devices.websocket.topics;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceKey;
import lt.kudze.stream.user.devices.websocket.messages.device.DeviceSubscriptionValue;
import lt.kudze.stream.user.devices.websocket.util.SystemUtil;
import org.apache.kafka.streams.kstream.Consumed;

public class DeviceAnomaliesSubscriptionTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_DEVICE_ANOMALIES_SUBSCRIBE_TOPIC");
    public static final Consumed<DeviceKey, DeviceSubscriptionValue> CONSUMED = Consumed.with(DeviceKey.serde, DeviceSubscriptionValue.serde);
}
