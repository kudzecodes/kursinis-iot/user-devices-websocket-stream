FROM openjdk:17-jdk-slim

ADD target/log4j.properties /log4j.properties
ADD target/lt.kudze.user.devices.websocket-0.1-jar-with-dependencies.jar /lt.kudze.user.devices.websocket-0.1-jar-with-dependencies.jar

CMD java -Dlog4j.configuration=file:/log4j.properties -jar /lt.kudze.user.devices.websocket-0.1-jar-with-dependencies.jar