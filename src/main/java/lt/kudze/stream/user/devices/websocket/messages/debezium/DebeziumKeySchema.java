package lt.kudze.stream.user.devices.websocket.messages.debezium;

public class DebeziumKeySchema {
    private final String type;
    private final DebeziumField[] fields;
    private final boolean optional;
    private final String name;

    public DebeziumKeySchema() {
        this(null, null, true, null);
    }

    public DebeziumKeySchema(String type, DebeziumField[] fields, boolean optional, String name) {
        this.type = type;
        this.fields = fields;
        this.optional = optional;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public DebeziumField[] getFields() {
        return fields;
    }

    public boolean isOptional() {
        return optional;
    }

    public String getName() {
        return name;
    }
}
