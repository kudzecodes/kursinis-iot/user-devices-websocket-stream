package lt.kudze.stream.user.devices.websocket.messages.output;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceUserValue;
import lt.kudze.stream.user.devices.websocket.messages.device.DeviceValue;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class OutputDeviceUser {
    public static final Serde<DeviceValue> serde = JsonSerdes.of(DeviceValue.class);

    private String role;
    private String createdAt;
    private String updatedAt;

    public OutputDeviceUser() {

    }

    public OutputDeviceUser(DeviceUserValue value) {
        this.role = value.getRole();
        this.createdAt = value.getCreatedAt();
        this.updatedAt = value.getUpdatedAt();
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
