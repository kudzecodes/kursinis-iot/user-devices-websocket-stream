package resources.input;

public class InputDeviceUsers {
    public static final String WITH_USERS_1_KEY = "input/users/withUsers/1.key.json";
    public static final String WITH_USERS_1_VALUE = "input/users/withUsers/1.json";
    public static final String WITH_USERS_1_DELETE_VALUE = "input/users/withUsers/1Delete.json";

    public static final String WITH_USERS_2_KEY = "input/users/withUsers/2.key.json";
    public static final String WITH_USERS_2_VALUE = "input/users/withUsers/2.json";
}
