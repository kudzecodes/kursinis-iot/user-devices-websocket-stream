package lt.kudze.stream.user.devices.websocket.util;

public class SystemUtil {

    /**
     * Just small utility that returns environment variable name if its not present value otherwise.
     *
     * @param name Environment variable name
     * @return Environment variable value, (in case of null, environment variable name is returned).
     */
    public static String getenv(String name) {
        return SystemUtil.getenv(name, name);
    }

    public static String getenv(String name, String _default) {
        String env = System.getenv(name);
        if (env == null)
            return _default;

        return env;
    }

}
