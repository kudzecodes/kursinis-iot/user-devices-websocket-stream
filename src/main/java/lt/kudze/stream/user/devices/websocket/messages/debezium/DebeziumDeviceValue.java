package lt.kudze.stream.user.devices.websocket.messages.debezium;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceValue;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumDeviceValue extends DebeziumValue<DeviceValue> {
    public static final Serde<DebeziumDeviceValue> serde = JsonSerdes.of(DebeziumDeviceValue.class);
}
