package lt.kudze.stream.user.devices.websocket.messages.output;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceValue;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

import java.util.HashMap;
import java.util.Map;

public class OutputDeviceValue extends DeviceValue {
    public static final Serde<OutputDeviceValue> serde = JsonSerdes.of(OutputDeviceValue.class);

    private Map<String, OutputDeviceUser> users;

    public OutputDeviceValue() {

    }

    public OutputDeviceValue(DeviceValue value, Map<String, OutputDeviceUser> users) {
        super(value);

        this.users = users == null ? new HashMap<>() : users;
    }

    public Map<String, OutputDeviceUser> getUsers() {
        return users;
    }
}
