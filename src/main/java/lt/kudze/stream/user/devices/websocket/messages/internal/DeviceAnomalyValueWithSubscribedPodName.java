package lt.kudze.stream.user.devices.websocket.messages.internal;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceAnomalyValue;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceAnomalyValueWithSubscribedPodName extends DeviceAnomalyValue {
    public static final Serde<DeviceAnomalyValueWithSubscribedPodName> serde = JsonSerdes.of(DeviceAnomalyValueWithSubscribedPodName.class);

    private String pod;

    public DeviceAnomalyValueWithSubscribedPodName() {

    }

    public DeviceAnomalyValueWithSubscribedPodName(DeviceAnomalyValue value, String pod) {
        super(value);

        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }
}
