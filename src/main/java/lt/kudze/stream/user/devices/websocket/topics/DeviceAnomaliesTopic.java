package lt.kudze.stream.user.devices.websocket.topics;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceAnomalyValue;
import lt.kudze.stream.user.devices.websocket.util.SystemUtil;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.Consumed;

public class DeviceAnomaliesTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_DEVICE_ANOMALIES_TOPIC");
    public static final Consumed<byte[], DeviceAnomalyValue> CONSUMED = Consumed.with(Serdes.ByteArray(), DeviceAnomalyValue.serde);
}
