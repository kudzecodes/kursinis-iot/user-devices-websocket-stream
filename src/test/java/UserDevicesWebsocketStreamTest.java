import lt.kudze.stream.user.devices.websocket.UserDevicesWsStream;
import lt.kudze.stream.user.devices.websocket.messages.device.*;
import lt.kudze.stream.user.devices.websocket.messages.internal.DeviceAnomalyValueWithSubscribedPodName;
import lt.kudze.stream.user.devices.websocket.messages.internal.DeviceSensorValueWithSubscribedPodName;
import lt.kudze.stream.user.devices.websocket.messages.internal.InternalDeviceSubscriptionValue;
import lt.kudze.stream.user.devices.websocket.messages.output.OutputDeviceValue;
import lt.kudze.stream.user.devices.websocket.topics.*;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import resources.input.InputDeviceUsers;
import resources.input.InputDevices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class UserDevicesWebsocketStreamTest {

    @Test
    public void testInternalDeviceSensorSubscriptionTable() throws IOException {
        System.out.println("Setup topology driver");
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9999"); // not existing server
        config.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                org.apache.kafka.streams.errors.LogAndContinueExceptionHandler.class.getName()
        );

        StreamsBuilder builder = new StreamsBuilder();
        UserDevicesWsStream.buildInternalDeviceSensorSubscriptionTable(builder).toStream().to("TEST");

        var topology = builder.build();
        var testDriver = new TopologyTestDriver(topology, config);

        var inputTopic = testDriver.createInputTopic(DeviceSensorSubscriptionTopic.NAME, DeviceKey.serde.serializer(), DeviceSubscriptionValue.serde.serializer());
        var outputTopic = testDriver.createOutputTopic("TEST", DeviceKey.serde.deserializer(), InternalDeviceSubscriptionValue.serde.deserializer());

        //At first topic should be empty.
        Assertions.assertTrue(outputTopic.isEmpty());

        //Lets produce some random subscriptions.
        //pod-a subscribes test1
        inputTopic.pipeInput(
                new DeviceKey("test1"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.ADD, "pod-a")
        );

        var record = outputTopic.readRecord();
        Assertions.assertEquals("test1", record.key().getUuid());
        Assertions.assertEquals(1, record.value().getPods().size());
        Assertions.assertTrue(record.value().getPods().contains("pod-a"));
        Assertions.assertTrue(outputTopic.isEmpty());

        //pod-a subscribes test2
        inputTopic.pipeInput(
                new DeviceKey("test2"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.ADD, "pod-a")
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("test2", record.key().getUuid());
        Assertions.assertEquals(1, record.value().getPods().size());
        Assertions.assertTrue(record.value().getPods().contains("pod-a"));
        Assertions.assertTrue(outputTopic.isEmpty());

        //pod-b subscribes test2
        inputTopic.pipeInput(
                new DeviceKey("test2"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.ADD, "pod-b")
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("test2", record.key().getUuid());
        Assertions.assertEquals(2, record.value().getPods().size());
        Assertions.assertTrue(record.value().getPods().contains("pod-a"));
        Assertions.assertTrue(record.value().getPods().contains("pod-b"));

        //Lets say pod-a unsubscribed test 1
        inputTopic.pipeInput(
                new DeviceKey("test1"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.REMOVE, "pod-a")
        );

        record = outputTopic.readRecord();
        Assertions.assertEquals("test1", record.key().getUuid());
        Assertions.assertEquals(0, record.value().getPods().size());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Lets say pod-b unsubscribed test 2
        inputTopic.pipeInput(
                new DeviceKey("test2"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.REMOVE, "pod-b")
        );

        record = outputTopic.readRecord();
        Assertions.assertEquals("test2", record.key().getUuid());
        Assertions.assertEquals(1, record.value().getPods().size());
        Assertions.assertTrue(record.value().getPods().contains("pod-a"));
        Assertions.assertTrue(outputTopic.isEmpty());

        testDriver.close();
    }

    @Test
    public void testInternalDeviceAnomaliesSubscriptionTable() throws IOException {
        System.out.println("Setup topology driver");
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9999"); // not existing server
        config.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                org.apache.kafka.streams.errors.LogAndContinueExceptionHandler.class.getName()
        );

        StreamsBuilder builder = new StreamsBuilder();
        UserDevicesWsStream.buildInternalDeviceAnomalySubscriptionTable(builder).toStream().to("TEST");

        var topology = builder.build();
        var testDriver = new TopologyTestDriver(topology, config);

        var inputTopic = testDriver.createInputTopic(DeviceAnomaliesSubscriptionTopic.NAME, DeviceKey.serde.serializer(), DeviceSubscriptionValue.serde.serializer());
        var outputTopic = testDriver.createOutputTopic("TEST", DeviceKey.serde.deserializer(), InternalDeviceSubscriptionValue.serde.deserializer());

        //At first topic should be empty.
        Assertions.assertTrue(outputTopic.isEmpty());

        //Lets produce some random subscriptions.
        //pod-a subscribes test1
        inputTopic.pipeInput(
                new DeviceKey("test1"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.ADD, "pod-a")
        );

        var record = outputTopic.readRecord();
        Assertions.assertEquals("test1", record.key().getUuid());
        Assertions.assertEquals(1, record.value().getPods().size());
        Assertions.assertTrue(record.value().getPods().contains("pod-a"));
        Assertions.assertTrue(outputTopic.isEmpty());

        //pod-a subscribes test2
        inputTopic.pipeInput(
                new DeviceKey("test2"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.ADD, "pod-a")
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("test2", record.key().getUuid());
        Assertions.assertEquals(1, record.value().getPods().size());
        Assertions.assertTrue(record.value().getPods().contains("pod-a"));
        Assertions.assertTrue(outputTopic.isEmpty());

        //pod-b subscribes test2
        inputTopic.pipeInput(
                new DeviceKey("test2"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.ADD, "pod-b")
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("test2", record.key().getUuid());
        Assertions.assertEquals(2, record.value().getPods().size());
        Assertions.assertTrue(record.value().getPods().contains("pod-a"));
        Assertions.assertTrue(record.value().getPods().contains("pod-b"));

        //Lets say pod-a unsubscribed test 1
        inputTopic.pipeInput(
                new DeviceKey("test1"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.REMOVE, "pod-a")
        );

        record = outputTopic.readRecord();
        Assertions.assertEquals("test1", record.key().getUuid());
        Assertions.assertEquals(0, record.value().getPods().size());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Lets say pod-b unsubscribed test 2
        inputTopic.pipeInput(
                new DeviceKey("test2"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.REMOVE, "pod-b")
        );

        record = outputTopic.readRecord();
        Assertions.assertEquals("test2", record.key().getUuid());
        Assertions.assertEquals(1, record.value().getPods().size());
        Assertions.assertTrue(record.value().getPods().contains("pod-a"));
        Assertions.assertTrue(outputTopic.isEmpty());

        testDriver.close();
    }

    @Test
    public void testEndToEnd() throws IOException {
        System.out.println("Setup topology driver");
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9999"); // not existing server
        config.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                org.apache.kafka.streams.errors.LogAndContinueExceptionHandler.class.getName()
        );

        var topology = UserDevicesWsStream.buildTopology().build();
        System.out.println(topology.describe());

        TopologyTestDriver testDriver = new TopologyTestDriver(topology, config);
//        testDriver.close();

        var inputDeviceTopic = testDriver.createInputTopic(DebeziumDeviceTopic.NAME, Serdes.String().serializer(), Serdes.String().serializer());
        var inputDeviceUserTopic = testDriver.createInputTopic(DebeziumDeviceUserTopic.NAME, Serdes.String().serializer(), Serdes.String().serializer());
        var outputTopic = testDriver.createOutputTopic(OutputDevicesTopic.NAME, Serdes.String().deserializer(), OutputDeviceValue.serde.deserializer());

        //Testing create with random device that does not have any sensors.
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.NO_USERS_KEY),
                getResource(InputDevices.NO_USERS_VALUE)
        );

        var record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cd", record.key());
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cd", record.value().getUuid());
        Assertions.assertEquals(0, record.value().getUsers().size());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getCreatedAt());
        Assertions.assertEquals("2024-05-02T06:20:47Z", record.value().getUpdatedAt());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Testing delete with random device that does not have any sensors.
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.NO_USERS_KEY),
                getResource(InputDevices.NO_USERS_DELETE_VALUE)
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cd", record.key());
        Assertions.assertNull(record.value());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Testing tombstone message.
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.NO_USERS_KEY),
                "null"
        );
        Assertions.assertTrue(outputTopic.isEmpty());

        //Testing device with users
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.WITH_USERS_KEY),
                getResource(InputDevices.WITH_USERS_VALUE)
        );

        record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.key());
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.value().getUuid());
        Assertions.assertEquals(0, record.value().getUsers().size());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getCreatedAt());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getUpdatedAt());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Lets add first sensor
        inputDeviceUserTopic.pipeInput(
                getResource(InputDeviceUsers.WITH_USERS_1_KEY),
                getResource(InputDeviceUsers.WITH_USERS_1_VALUE)
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.key());
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.value().getUuid());
        Assertions.assertEquals(1, record.value().getUsers().size());
        Assertions.assertTrue(record.value().getUsers().containsKey("9b528b29-b3f3-4b4e-888c-8bb2673d79ac"));
        Assertions.assertEquals("owner", record.value().getUsers().get("9b528b29-b3f3-4b4e-888c-8bb2673d79ac").getRole());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getCreatedAt());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getUpdatedAt());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Lets add second user
        inputDeviceUserTopic.pipeInput(
                getResource(InputDeviceUsers.WITH_USERS_2_KEY),
                getResource(InputDeviceUsers.WITH_USERS_2_VALUE)
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.key());
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.value().getUuid());
        Assertions.assertEquals(2, record.value().getUsers().size());
        Assertions.assertTrue(record.value().getUsers().containsKey("9b528b29-b3f3-4b4e-888c-8bb2673d79ac"));
        Assertions.assertEquals("owner", record.value().getUsers().get("9b528b29-b3f3-4b4e-888c-8bb2673d79ac").getRole());
        Assertions.assertTrue(record.value().getUsers().containsKey("9b528b29-b3f3-4b4e-888c-8bb2673d79ad"));
        Assertions.assertEquals("view", record.value().getUsers().get("9b528b29-b3f3-4b4e-888c-8bb2673d79ad").getRole());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getCreatedAt());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getUpdatedAt());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Lets remove first sensor
        inputDeviceUserTopic.pipeInput(
                getResource(InputDeviceUsers.WITH_USERS_1_KEY),
                getResource(InputDeviceUsers.WITH_USERS_1_DELETE_VALUE)
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.key());
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.value().getUuid());
        Assertions.assertEquals(2, record.value().getUsers().size());
        Assertions.assertTrue(record.value().getUsers().containsKey("9b528b29-b3f3-4b4e-888c-8bb2673d79ac"));
        Assertions.assertNull(record.value().getUsers().get("9b528b29-b3f3-4b4e-888c-8bb2673d79ac"));
        Assertions.assertTrue(record.value().getUsers().containsKey("9b528b29-b3f3-4b4e-888c-8bb2673d79ad"));
        Assertions.assertEquals("view", record.value().getUsers().get("9b528b29-b3f3-4b4e-888c-8bb2673d79ad").getRole());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getCreatedAt());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getUpdatedAt());
        Assertions.assertTrue(outputTopic.isEmpty());

        inputDeviceUserTopic.pipeInput(
                getResource(InputDeviceUsers.WITH_USERS_1_KEY),
                "null"
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.key());
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.value().getUuid());
        Assertions.assertEquals(1, record.value().getUsers().size());
        Assertions.assertTrue(record.value().getUsers().containsKey("9b528b29-b3f3-4b4e-888c-8bb2673d79ad"));
        Assertions.assertEquals("view", record.value().getUsers().get("9b528b29-b3f3-4b4e-888c-8bb2673d79ad").getRole());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getCreatedAt());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getUpdatedAt());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Just some random update that should be skipped
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.WITH_USERS_KEY),
                getResource(InputDevices.WITH_USERS_UPDATE_SKIP_VALUE)
        );
        Assertions.assertTrue(outputTopic.isEmpty());


        //After the top one lets test device sensor end to end.
        var inputSensorSubscribeTopic = testDriver.createInputTopic(DeviceSensorSubscriptionTopic.NAME, DeviceKey.serde.serializer(), DeviceSubscriptionValue.serde.serializer());
        var inputDeviceSensorLogTopic = testDriver.createInputTopic(DeviceSensorTopic.NAME, DeviceSensorKey.serde.serializer(), DeviceSensorValue.serde.serializer());

        //Lets say pod-a and pod-b subscribes test device
        inputSensorSubscribeTopic.pipeInput(
                new DeviceKey("test"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.ADD, "pod-a")
        );

        inputSensorSubscribeTopic.pipeInput(
                new DeviceKey("test"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.ADD, "pod-b")
        );

        var outputSensorPodATopic = testDriver.createOutputTopic(OutputDeviceSensorLogTopic.getName("pod-a"), DeviceSensorKey.serde.deserializer(), DeviceSensorValueWithSubscribedPodName.serde.deserializer());
        var outputSensorPodBTopic = testDriver.createOutputTopic(OutputDeviceSensorLogTopic.getName("pod-b"), DeviceSensorKey.serde.deserializer(), DeviceSensorValueWithSubscribedPodName.serde.deserializer());

        //Pipe in some test
        inputDeviceSensorLogTopic.pipeInput(
                new DeviceSensorKey("test", "date"),
                new DeviceSensorValue("{\"randomSensor\": [10]}")
        );

        var record2 = outputSensorPodATopic.readRecord();
        Assertions.assertEquals("test", record2.key().getDeviceUuid());
        Assertions.assertEquals("date", record2.key().getDate());
        Assertions.assertEquals("{\"randomSensor\": [10]}", record2.value().getJson());
        Assertions.assertEquals("pod-a", record2.value().getPod());

        record2 = outputSensorPodBTopic.readRecord();
        Assertions.assertEquals("test", record2.key().getDeviceUuid());
        Assertions.assertEquals("date", record2.key().getDate());
        Assertions.assertEquals("{\"randomSensor\": [10]}", record2.value().getJson());
        Assertions.assertEquals("pod-b", record2.value().getPod());

        Assertions.assertTrue(outputSensorPodATopic.isEmpty());
        Assertions.assertTrue(outputSensorPodBTopic.isEmpty());

        //After the top one lets test device anomaly end to end.
        var inputAnomalySubscribeTopic = testDriver.createInputTopic(DeviceAnomaliesSubscriptionTopic.NAME, DeviceKey.serde.serializer(), DeviceSubscriptionValue.serde.serializer());
        var inputDeviceAnomalyTopic = testDriver.createInputTopic(DeviceAnomaliesTopic.NAME, Serdes.String().serializer(), DeviceAnomalyValue.serde.serializer());

        //Lets say pod-a and pod-b subscribes test device
        inputAnomalySubscribeTopic.pipeInput(
                new DeviceKey("test"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.ADD, "pod-a")
        );

        inputAnomalySubscribeTopic.pipeInput(
                new DeviceKey("test"),
                new DeviceSubscriptionValue(DeviceSubscriptionAction.ADD, "pod-b")
        );

        var outputAnomalyPodATopic = testDriver.createOutputTopic(OutputDeviceAnomaliesTopic.getName("pod-a"), Serdes.String().deserializer(), DeviceAnomalyValueWithSubscribedPodName.serde.deserializer());
        var outputAnomalyPodBTopic = testDriver.createOutputTopic(OutputDeviceAnomaliesTopic.getName("pod-b"), Serdes.String().deserializer(), DeviceAnomalyValueWithSubscribedPodName.serde.deserializer());

        //Pipe in some test
        inputDeviceAnomalyTopic.pipeInput(
                "null",
                new DeviceAnomalyValue("test", "date")
        );

        var record3 = outputAnomalyPodATopic.readRecord();
        Assertions.assertNull(record3.key());
        Assertions.assertEquals("test", record3.value().getDeviceUuid());
        Assertions.assertEquals("date", record3.value().getDate());
        Assertions.assertEquals("pod-a", record3.value().getPod());

        record3 = outputAnomalyPodBTopic.readRecord();
        Assertions.assertNull(record3.key());
        Assertions.assertEquals("test", record3.value().getDeviceUuid());
        Assertions.assertEquals("date", record3.value().getDate());
        Assertions.assertEquals("pod-b", record3.value().getPod());

        Assertions.assertTrue(outputSensorPodATopic.isEmpty());
        Assertions.assertTrue(outputSensorPodBTopic.isEmpty());

        testDriver.close();
    }

    private String getResource(String resource) throws IOException {
        return Files.readString(Path.of(Objects.requireNonNull(this.getClass().getResource("/messages/" + resource)).getPath()));
    }
}
