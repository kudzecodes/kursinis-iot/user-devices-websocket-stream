package lt.kudze.stream.user.devices.websocket.messages.device;

import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceValue {
    public static final Serde<DeviceValue> serde = JsonSerdes.of(DeviceValue.class);

    private String uuid;
    private String createdAt;
    private String updatedAt;

    public DeviceValue() {

    }

    public DeviceValue(DeviceValue device) {
        this.uuid = device.uuid;
        this.createdAt = device.createdAt;
        this.updatedAt = device.updatedAt;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
