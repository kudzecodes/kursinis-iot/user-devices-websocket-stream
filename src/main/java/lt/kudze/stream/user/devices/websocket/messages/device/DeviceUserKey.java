package lt.kudze.stream.user.devices.websocket.messages.device;

import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceUserKey {
    public static final Serde<DeviceValue> serde = JsonSerdes.of(DeviceValue.class);

    private String deviceId;
    private String userId;

    public DeviceUserKey() {
        this(null, null);
    }

    public DeviceUserKey(String deviceId, String userId) {
        this.deviceId = deviceId;
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
