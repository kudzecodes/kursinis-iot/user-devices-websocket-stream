package lt.kudze.stream.user.devices.websocket.messages.debezium;

public abstract class DebeziumValue<DataClass> {
    private final DebeziumValueSchema schema;
    private final DebeziumValuePayload<DataClass> payload;

    public DebeziumValue() {
        this(null, null);
    }

    public DebeziumValue(DebeziumValueSchema schema, DebeziumValuePayload<DataClass> payload) {
        this.schema = schema;
        this.payload = payload;
    }

    public DebeziumValueSchema getSchema() {
        return schema;
    }

    public DebeziumValuePayload<DataClass> getPayload() {
        return payload;
    }
}
