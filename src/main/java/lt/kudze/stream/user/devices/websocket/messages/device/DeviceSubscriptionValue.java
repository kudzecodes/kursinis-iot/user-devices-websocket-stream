package lt.kudze.stream.user.devices.websocket.messages.device;

import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceSubscriptionValue {
    public static final Serde<DeviceSubscriptionValue> serde = JsonSerdes.of(DeviceSubscriptionValue.class);

    private DeviceSubscriptionAction action;
    private String pod;

    public DeviceSubscriptionValue() {

    }

    public DeviceSubscriptionValue(DeviceSubscriptionAction action, String pod) {
        this.action = action;
        this.pod = pod;
    }

    public DeviceSubscriptionAction getAction() {
        return action;
    }

    public void setAction(DeviceSubscriptionAction action) {
        this.action = action;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }
}
