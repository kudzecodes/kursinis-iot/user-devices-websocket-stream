package lt.kudze.stream.user.devices.websocket.messages.debezium;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceKey;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumDeviceKey extends DebeziumKey<DeviceKey> {
    public static final Serde<DebeziumDeviceKey> serde = JsonSerdes.of(DebeziumDeviceKey.class);
}
