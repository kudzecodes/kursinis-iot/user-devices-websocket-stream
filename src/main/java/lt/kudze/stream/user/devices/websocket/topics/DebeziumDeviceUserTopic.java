package lt.kudze.stream.user.devices.websocket.topics;

import lt.kudze.stream.user.devices.websocket.messages.debezium.DebeziumDeviceUserKey;
import lt.kudze.stream.user.devices.websocket.messages.debezium.DebeziumDeviceUserValue;
import lt.kudze.stream.user.devices.websocket.util.SystemUtil;
import org.apache.kafka.streams.kstream.Consumed;

public class DebeziumDeviceUserTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_DEBEZIUM_DEVICE_USER_TOPIC");
    public static final Consumed<DebeziumDeviceUserKey, DebeziumDeviceUserValue> CONSUMED = Consumed.with(DebeziumDeviceUserKey.serde, DebeziumDeviceUserValue.serde);
}
