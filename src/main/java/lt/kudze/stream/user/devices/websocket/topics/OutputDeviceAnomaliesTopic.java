package lt.kudze.stream.user.devices.websocket.topics;

import lt.kudze.stream.user.devices.websocket.messages.internal.DeviceAnomalyValueWithSubscribedPodName;
import lt.kudze.stream.user.devices.websocket.util.SystemUtil;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.Produced;

public class OutputDeviceAnomaliesTopic {
    private static final String PREFIX = SystemUtil.getenv("KAFKA_OUTPUT_DEVICE_ANOMALIES_TOPIC_PREFIX");

    public static String getName(String pod) {
        return PREFIX + pod;
    }

    public static final Produced<byte[], DeviceAnomalyValueWithSubscribedPodName> PRODUCED = Produced.with(Serdes.ByteArray(), DeviceAnomalyValueWithSubscribedPodName.serde);
}
