package lt.kudze.stream.user.devices.websocket.messages.device;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum DeviceSubscriptionAction {
    @JsonProperty("add")
    ADD,
    @JsonProperty("remove")
    REMOVE
}
