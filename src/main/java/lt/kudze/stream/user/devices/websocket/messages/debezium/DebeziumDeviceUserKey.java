package lt.kudze.stream.user.devices.websocket.messages.debezium;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceUserKey;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumDeviceUserKey extends DebeziumKey<DeviceUserKey> {
    public static final Serde<DebeziumDeviceUserKey> serde = JsonSerdes.of(DebeziumDeviceUserKey.class);
}
