package resources.input;

public class InputDevices {
    public static final String NO_USERS_KEY = "input/devices/noUsers.key.json";
    public static final String NO_USERS_VALUE = "input/devices/noUsers.json";
    public static final String NO_USERS_DELETE_VALUE = "input/devices/noUsersDelete.json";

    public static final String WITH_USERS_KEY = "input/devices/withUsers.key.json";
    public static final String WITH_USERS_VALUE = "input/devices/withUsers.json";
    public static final String WITH_USERS_UPDATE_SKIP_VALUE = "input/devices/withUsersUpdateSkip.json";
}
