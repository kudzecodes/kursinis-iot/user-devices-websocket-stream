package lt.kudze.stream.user.devices.websocket.messages.debezium;

import java.util.Optional;

public class DebeziumValuePayload<DataClass> {
    protected final DataClass before;
    protected final DataClass after;

    public DebeziumValuePayload() {
        this(null, null);
    }

    public DebeziumValuePayload(DataClass before, DataClass after) {
        this.before = before;
        this.after = after;
    }

    public Optional<DataClass> getBefore() {
        return Optional.ofNullable(before);
    }

    public Optional<DataClass> getAfter() {
        return Optional.ofNullable(after);
    }
}
