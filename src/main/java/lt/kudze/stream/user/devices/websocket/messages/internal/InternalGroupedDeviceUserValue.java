package lt.kudze.stream.user.devices.websocket.messages.internal;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceUserValue;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class InternalGroupedDeviceUserValue extends AggregatedValues<DeviceUserValue> {
    public static final Serde<InternalGroupedDeviceUserValue> serde = JsonSerdes.of(InternalGroupedDeviceUserValue.class);
}
