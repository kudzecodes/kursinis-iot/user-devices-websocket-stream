package lt.kudze.stream.user.devices.websocket.messages.device;

import lt.kudze.stream.user.devices.websocket.serdes.StringSerdes;
import org.apache.kafka.common.serialization.Serde;

public class PodNameKey {
    public static final Serde<PodNameKey> serde = StringSerdes.from(
            PodNameKey::toString,
            PodNameKey::new
    );

    private String podName;

    public PodNameKey() {
        this(null);
    }

    public PodNameKey(String podName) {
        this.podName = podName;
    }

    public String getPodName() {
        return podName;
    }

    public void setPodName(String podName) {
        this.podName = podName;
    }

    @Override
    public String toString() {
        return this.podName;
    }
}
