package lt.kudze.stream.user.devices.websocket.messages.device;

import com.fasterxml.jackson.annotation.JsonProperty;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceAnomalyValue {
    public static final Serde<DeviceAnomalyValue> serde = JsonSerdes.of(DeviceAnomalyValue.class);

    @JsonProperty("deviceUuid")
    private String deviceUuid;
    private String date;

    public DeviceAnomalyValue() {

    }

    public DeviceAnomalyValue(String deviceUuid, String date) {
        this.deviceUuid = deviceUuid;
        this.date = date;
    }

    public DeviceAnomalyValue(DeviceAnomalyValue value) {
        this.deviceUuid = value.deviceUuid;
        this.date = value.date;
    }

    public String getDeviceUuid() {
        return deviceUuid;
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
