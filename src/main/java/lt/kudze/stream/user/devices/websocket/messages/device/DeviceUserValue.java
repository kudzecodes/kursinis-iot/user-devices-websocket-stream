package lt.kudze.stream.user.devices.websocket.messages.device;

import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceUserValue {
    public static final Serde<DeviceValue> serde = JsonSerdes.of(DeviceValue.class);

    private String deviceId;
    private String userId;
    private String role;
    private String createdAt;
    private String updatedAt;

    public DeviceUserValue() {

    }

    public DeviceUserValue(DeviceUserValue value) {
        this.deviceId = value.deviceId;
        this.userId = value.userId;
        this.role = value.role;
        this.createdAt = value.createdAt;
        this.updatedAt = value.updatedAt;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
