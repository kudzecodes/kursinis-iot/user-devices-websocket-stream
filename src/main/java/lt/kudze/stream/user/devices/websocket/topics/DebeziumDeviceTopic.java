package lt.kudze.stream.user.devices.websocket.topics;

import lt.kudze.stream.user.devices.websocket.messages.debezium.DebeziumDeviceKey;
import lt.kudze.stream.user.devices.websocket.messages.debezium.DebeziumDeviceValue;
import lt.kudze.stream.user.devices.websocket.util.SystemUtil;
import org.apache.kafka.streams.kstream.Consumed;

public class DebeziumDeviceTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_DEBEZIUM_DEVICE_TOPIC");
    public static final Consumed<DebeziumDeviceKey, DebeziumDeviceValue> CONSUMED = Consumed.with(DebeziumDeviceKey.serde, DebeziumDeviceValue.serde);
}
