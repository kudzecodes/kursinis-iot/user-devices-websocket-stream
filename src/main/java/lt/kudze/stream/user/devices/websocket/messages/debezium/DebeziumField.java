package lt.kudze.stream.user.devices.websocket.messages.debezium;

public class DebeziumField {
    private final String type;
    private final boolean optional;
    private final String field;

    public DebeziumField() {
        this(null, true, null);
    }

    public DebeziumField(String type, boolean optional, String field) {
        this.type = type;
        this.optional = optional;
        this.field = field;
    }

    public String getType() {
        return type;
    }

    public boolean isOptional() {
        return optional;
    }

    public String getField() {
        return field;
    }
}
