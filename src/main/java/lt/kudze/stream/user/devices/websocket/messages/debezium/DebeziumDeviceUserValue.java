package lt.kudze.stream.user.devices.websocket.messages.debezium;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceUserValue;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumDeviceUserValue extends DebeziumValue<DeviceUserValue> {
    public static final Serde<DebeziumDeviceUserValue> serde = JsonSerdes.of(DebeziumDeviceUserValue.class);
}
