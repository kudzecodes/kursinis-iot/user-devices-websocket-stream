package lt.kudze.stream.user.devices.websocket.topics;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceKey;
import lt.kudze.stream.user.devices.websocket.messages.output.OutputDeviceValue;
import lt.kudze.stream.user.devices.websocket.util.SystemUtil;
import org.apache.kafka.streams.kstream.Produced;

public class OutputDevicesTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_OUTPUT_DEVICES_TOPIC");
    public static final Produced<DeviceKey, OutputDeviceValue> PRODUCED = Produced.with(DeviceKey.serde, OutputDeviceValue.serde);
}
