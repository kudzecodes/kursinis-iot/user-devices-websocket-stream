package lt.kudze.stream.user.devices.websocket.messages.internal;

import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

import java.util.TreeSet;

public class InternalDeviceSubscriptionValue {
    public static final Serde<InternalDeviceSubscriptionValue> serde = JsonSerdes.of(InternalDeviceSubscriptionValue.class);

    private TreeSet<String> pods;

    public InternalDeviceSubscriptionValue() {
        this(new TreeSet<>());
    }

    public InternalDeviceSubscriptionValue(TreeSet<String> pods)
    {
        this.pods = pods;
    }

    public TreeSet<String> getPods() {
        return pods;
    }

    public void setPods(TreeSet<String> pods) {
        this.pods = pods;
    }
}
