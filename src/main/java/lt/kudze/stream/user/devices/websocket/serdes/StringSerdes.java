package lt.kudze.stream.user.devices.websocket.serdes;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.function.Function;

public class StringSerdes {
    public static <T> Serde<T> from(Function<T, String> toString, Function<String, T> fromString) {
        return Serdes.serdeFrom(
                (topic, data) -> {
                    if (data == null) {
                        return null;
                    }
                    String str = toString.apply(data);
                    return Serdes.String().serializer().serialize(topic, str);
                },
                (topic, data) -> {
                    if (data == null) {
                        return null;
                    }
                    String str = Serdes.String().deserializer().deserialize(topic, data);
                    return fromString.apply(str);
                }
        );
    }
}
