package lt.kudze.stream.user.devices.websocket.messages.internal;

import lt.kudze.stream.user.devices.websocket.messages.debezium.DebeziumValuePayload;
import lt.kudze.stream.user.devices.websocket.messages.device.DeviceUserKey;
import lt.kudze.stream.user.devices.websocket.messages.device.DeviceUserValue;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

import java.util.Optional;

public class InternalDeviceUserValue {
    public static final Serde<InternalDeviceUserValue> serde = JsonSerdes.of(InternalDeviceUserValue.class);

    private DeviceUserKey key;
    private DebeziumValuePayload<DeviceUserValue> payload;

    public InternalDeviceUserValue() {

    }

    public InternalDeviceUserValue(
            DeviceUserKey key,
            DebeziumValuePayload<DeviceUserValue> payload
    ) {
        this.key = key;
        this.payload = payload;
    }

    public DeviceUserKey getKey() {
        return key;
    }

    public Optional<DebeziumValuePayload<DeviceUserValue>> getPayload() {
        return Optional.ofNullable(payload);
    }
}
