package lt.kudze.stream.user.devices.websocket.messages.internal;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceSensorValue;
import lt.kudze.stream.user.devices.websocket.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceSensorValueWithSubscribedPodName extends DeviceSensorValue {
    public static final Serde<DeviceSensorValueWithSubscribedPodName> serde = JsonSerdes.of(DeviceSensorValueWithSubscribedPodName.class);

    private String pod;

    public DeviceSensorValueWithSubscribedPodName() {

    }

    public DeviceSensorValueWithSubscribedPodName(DeviceSensorValue value, String pod) {
        super(value);

        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }
}
