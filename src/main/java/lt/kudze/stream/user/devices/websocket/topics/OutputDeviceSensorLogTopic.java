package lt.kudze.stream.user.devices.websocket.topics;

import lt.kudze.stream.user.devices.websocket.messages.device.DeviceSensorKey;
import lt.kudze.stream.user.devices.websocket.messages.internal.DeviceSensorValueWithSubscribedPodName;
import lt.kudze.stream.user.devices.websocket.util.SystemUtil;
import org.apache.kafka.streams.kstream.Produced;

public class OutputDeviceSensorLogTopic {
    private static final String PREFIX = SystemUtil.getenv("KAFKA_OUTPUT_DEVICE_SENSOR_LOG_TOPIC_PREFIX");

    public static String getName(String pod) {
        return PREFIX + pod;
    }

    public static final Produced<DeviceSensorKey, DeviceSensorValueWithSubscribedPodName> PRODUCED = Produced.with(DeviceSensorKey.serde, DeviceSensorValueWithSubscribedPodName.serde);
}
