/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package lt.kudze.stream.user.devices.websocket;

import lt.kudze.stream.user.devices.websocket.messages.debezium.DebeziumValue;
import lt.kudze.stream.user.devices.websocket.messages.device.*;
import lt.kudze.stream.user.devices.websocket.messages.internal.*;
import lt.kudze.stream.user.devices.websocket.messages.output.OutputDeviceUser;
import lt.kudze.stream.user.devices.websocket.messages.output.OutputDeviceValue;
import lt.kudze.stream.user.devices.websocket.topics.*;
import lt.kudze.stream.user.devices.websocket.util.SystemUtil;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class UserDevicesWsStream {

    public static KTable<DeviceKey, DeviceValue> buildInternalDeviceTable(StreamsBuilder builder) {
        var debeziumDeviceStream = builder.stream(DebeziumDeviceTopic.NAME, DebeziumDeviceTopic.CONSUMED);

        //It is given that deviceUuid cannot change.
        return debeziumDeviceStream.filter(
                (key, value) -> value != null,
                Named.as("devices-filter-tombstone")
        ).mapValues(
                DebeziumValue::getPayload,
                Named.as("devices-map-payload")
        ).filter(
                //Since we only care about uuid, we only need to handle insert or delete.
                //It is given that device uuid will not change.
                (key, value) -> value.getBefore().isEmpty() || value.getAfter().isEmpty(),
                Named.as("devices-filter-spam-updates")
        ).map(
                (key, value) -> new KeyValue<>(
                        key.getPayload(),
                        value.getAfter().orElse(null)
                ),
                Named.as("devices-map-to-after")
        ).toTable(
                Named.as("devices-to-table"),
                Materialized.<DeviceKey, DeviceValue, KeyValueStore<Bytes, byte[]>>as("devices-table")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(DeviceValue.serde)
        );
    }

    public static KTable<DeviceKey, InternalGroupedDeviceUserValue> buildInternalDeviceUserTable(StreamsBuilder builder) {
        var debeziumDeviceUserStream = builder.stream(DebeziumDeviceUserTopic.NAME, DebeziumDeviceUserTopic.CONSUMED);

        //It is given that deviceSensorUuid cannot change.
        //It is given that deviceUuid cannot change.
        return debeziumDeviceUserStream.map(
                (key, value) -> new KeyValue<>(
                        new DeviceKey(key.getPayload().getDeviceId()),
                        new InternalDeviceUserValue(
                                key.getPayload(),
                                value == null ? null : value.getPayload()
                        )
                ),
                Named.as("users-key-to-device-map-to-internal")
        ).groupByKey(Grouped.with(
                "internal-users-grouped",
                DeviceKey.serde, InternalDeviceUserValue.serde
        )).aggregate(
                InternalGroupedDeviceUserValue::new,
                (aggKey, newValue, aggValue) -> {
                    var userId = newValue.getKey().getUserId();
                    var payloadOptional = newValue.getPayload();

                    //If tombstone, then we must remove from the devices to delete.
                    if (payloadOptional.isEmpty()) {
                        aggValue.getValues().remove(userId);
                        return aggValue;
                    }

                    //If delete, then lets add null to aggregated value. (will mark for deletion)
                    var payload = payloadOptional.get();
                    if (payload.getAfter().isEmpty()) {
                        aggValue.getValues().put(userId, null);
                        return aggValue;
                    }

                    //Otherwise we just have to insert/update the value.
                    aggValue.getValues().put(userId, payload.getAfter().get());

                    return aggValue;
                },
                Named.as("internal-users-keyed-to-devices-aggregated"),
                Materialized.<DeviceKey, InternalGroupedDeviceUserValue, KeyValueStore<Bytes, byte[]>>as("internal-users-keyed-to-devices-aggregated")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(InternalGroupedDeviceUserValue.serde)
        );
    }

    public static KTable<DeviceKey, OutputDeviceValue> buildOutputDeviceTable(StreamsBuilder builder) {
        var internalDeviceTable = buildInternalDeviceTable(builder);
        var internalDeviceSensorTable = buildInternalDeviceUserTable(builder);

        return internalDeviceTable.leftJoin(
                internalDeviceSensorTable,
                (device, users) -> new OutputDeviceValue(
                        device,
                        users == null ? null : users.getValues().entrySet().stream().collect(
                                HashMap::new,
                                (m, entry) -> m.put(entry.getKey(), entry.getValue() == null ? null : new OutputDeviceUser(entry.getValue())),
                                HashMap::putAll
                        )
                ),
                Named.as("device-with-users-table"),
                Materialized.<DeviceKey, OutputDeviceValue, KeyValueStore<Bytes, byte[]>>
                                as("device-with-users-table")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(OutputDeviceValue.serde)
        );
    }

    public static KTable<DeviceKey, InternalDeviceSubscriptionValue> buildInternalDeviceSubscriptionTable(
            KStream<DeviceKey, DeviceSubscriptionValue> deviceSubscriptions,
            String type
    ) {
        return deviceSubscriptions.groupByKey(
                Grouped.<DeviceKey, DeviceSubscriptionValue>as("device-" + type + "-subscriptions-grouped")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(DeviceSubscriptionValue.serde)
        ).aggregate(
                InternalDeviceSubscriptionValue::new,
                (aggKey, newValue, aggValue) -> {
                    switch (newValue.getAction()) {
                        case ADD:
                            aggValue.getPods().add(newValue.getPod());
                            break;
                        case REMOVE:
                            aggValue.getPods().remove(newValue.getPod());
                    }

                    return aggValue;
                },
                Named.as("device-" + type + "-subscriptions-aggregated-to-internal"),
                Materialized.<DeviceKey, InternalDeviceSubscriptionValue, KeyValueStore<Bytes, byte[]>>as("device-" + type + "-subscriptions-aggregated-to-internal")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(InternalDeviceSubscriptionValue.serde)
        );
    }

    public static KTable<DeviceKey, InternalDeviceSubscriptionValue> buildInternalDeviceSensorSubscriptionTable(
            StreamsBuilder builder
    ) {
        return buildInternalDeviceSubscriptionTable(
                builder.stream(DeviceSensorSubscriptionTopic.NAME, DeviceSensorSubscriptionTopic.CONSUMED),
                "sensor"
        );
    }

    public static KTable<DeviceKey, InternalDeviceSubscriptionValue> buildInternalDeviceAnomalySubscriptionTable(
            StreamsBuilder builder
    ) {
        return buildInternalDeviceSubscriptionTable(
                builder.stream(DeviceAnomaliesSubscriptionTopic.NAME, DeviceAnomaliesSubscriptionTopic.CONSUMED),
                "anomalies"
        );
    }

    public static void buildDeviceSensorTopology(StreamsBuilder builder) {
        var internalDeviceSubscriptionTable = buildInternalDeviceSensorSubscriptionTable(builder);

        //We have kstream of device sensors coming in.
        var deviceSensors = builder.stream(DeviceSensorTopic.NAME, DeviceSensorTopic.CONSUMED);

        deviceSensors.map(
                (key, value) -> new KeyValue<>(
                        new DeviceKey(key.getDeviceUuid()),
                        new InternalDeviceSensorValue(value, key.getDate())
                ),
                Named.as("device-sensors-map-to-internal")
        ).join(
                internalDeviceSubscriptionTable,
                DeviceSensorValueWithSubscribedPodNames::new,
                Joined.<DeviceKey, InternalDeviceSensorValue, InternalDeviceSubscriptionValue>as("internal-device-sensors-joined-to-subscribed-map")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(InternalDeviceSensorValue.serde)
        ).flatMap(
                (key, value) -> {
                    var result = new LinkedList<KeyValue<DeviceSensorKey, DeviceSensorValueWithSubscribedPodName>>();

                    for (var podName : value.getPods())
                        result.add(new KeyValue<>(
                                new DeviceSensorKey(key.getUuid(), value.getDate()),
                                new DeviceSensorValueWithSubscribedPodName(value, podName))
                        );

                    return result;
                },
                Named.as("internal-device-sensors-flatmap-by-pod")
        ).to(
                (key, value, context) -> OutputDeviceSensorLogTopic.getName(value.getPod()),
                OutputDeviceSensorLogTopic.PRODUCED
        );
    }

    public static void buildDeviceAnomaliesTopology(StreamsBuilder builder) {
        var internalDeviceSubscriptionTable = buildInternalDeviceAnomalySubscriptionTable(builder);

        //We have kstream of device anomalies coming in.
        var deviceAnomalies = builder.stream(DeviceAnomaliesTopic.NAME, DeviceAnomaliesTopic.CONSUMED);

        deviceAnomalies.selectKey(
                (key, value) -> new DeviceKey(value.getDeviceUuid()),
                Named.as("device-anomalies-key-to-device")
        ).join(
                internalDeviceSubscriptionTable,
                DeviceAnomalyValueWithSubscribedPodNames::new,
                Joined.<DeviceKey, DeviceAnomalyValue, InternalDeviceSubscriptionValue>as("device-anomalies-joined-to-subscribed-map")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(DeviceAnomalyValue.serde)
        ).flatMap(
                (key, value) -> {
                    var result = new LinkedList<KeyValue<byte[], DeviceAnomalyValueWithSubscribedPodName>>();

                    for (var podName : value.getPods())
                        result.add(new KeyValue<>(
                                null,
                                new DeviceAnomalyValueWithSubscribedPodName(value, podName))
                        );

                    return result;
                },
                Named.as("device-anomalies-flatmap-by-pod")
        ).to(
                (key, value, context) -> OutputDeviceAnomaliesTopic.getName(value.getPod()),
                OutputDeviceAnomaliesTopic.PRODUCED
        );
    }

    public static StreamsBuilder buildTopology() {
        StreamsBuilder builder = new StreamsBuilder();

        buildOutputDeviceTable(builder).toStream(
                Named.as("output-to-stream")
        ).to(
                OutputDevicesTopic.NAME,
                OutputDevicesTopic.PRODUCED
        );

        buildDeviceSensorTopology(builder);
        buildDeviceAnomaliesTopology(builder);

        return builder;
    }

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, SystemUtil.getenv("KAFKA_GROUP_ID"));
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, SystemUtil.getenv("KAFKA_BROKERS"));
        props.put(StreamsConfig.SECURITY_PROTOCOL_CONFIG, SystemUtil.getenv("KAFKA_SECURITY_PROTOCOL", "PLAINTEXT"));
        props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, SystemUtil.getenv("KAFKA_TRUSTSTORE_LOCATION", null));
        props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, SystemUtil.getenv("KAFKA_TRUSTSTORE_PASSWORD", null));
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 100);
        props.put(StreamsConfig.producerPrefix(ProducerConfig.LINGER_MS_CONFIG), 0);

        final StreamsBuilder builder = UserDevicesWsStream.buildTopology();

        final Topology topology = builder.build();
        System.out.println(topology.describe());

        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}
