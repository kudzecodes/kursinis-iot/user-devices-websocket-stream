package lt.kudze.stream.user.devices.websocket.messages.device;

import lt.kudze.stream.user.devices.websocket.serdes.StringSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceKey {
    public static final Serde<DeviceKey> serde = StringSerdes.from(
            DeviceKey::toString,
            DeviceKey::new
    );

    private String uuid;

    public DeviceKey() {
        this(null);
    }

    public DeviceKey(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return this.uuid;
    }
}
