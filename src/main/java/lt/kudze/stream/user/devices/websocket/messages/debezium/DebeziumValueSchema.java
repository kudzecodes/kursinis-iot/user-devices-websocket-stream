package lt.kudze.stream.user.devices.websocket.messages.debezium;

public class DebeziumValueSchema extends DebeziumKeySchema {
    private final String type;
    private final DebeziumField[] fields;

    public DebeziumValueSchema() {
        this(null, null);
    }

    public DebeziumValueSchema(String type, DebeziumField[] fields) {
        this.type = type;
        this.fields = fields;
    }

    public String getType() {
        return type;
    }

    public DebeziumField[] getFields() {
        return fields;
    }
}
